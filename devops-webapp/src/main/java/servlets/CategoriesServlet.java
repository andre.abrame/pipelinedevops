package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Categorie;
import services.DevOpsService;

@WebServlet("/categories")
public class CategoriesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DevOpsService dos = new DevOpsService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Categorie> categories = dos.findAllCategorie();
		request.setAttribute("categories", categories);
		request.getServletContext()
			.getRequestDispatcher("/WEB-INF/views/categories.jsp")
			.forward(request, response);
	}

}
