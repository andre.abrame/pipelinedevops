package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Categorie;
import models.Outil;
import services.DevOpsService;

@WebServlet("/addCategorie")
public class AddCategorieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	DevOpsService dos = new DevOpsService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getServletContext()
			.getRequestDispatcher("/WEB-INF/views/addCategorie.jsp")
			.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String description = request.getParameter("description");
		Categorie c = new Categorie(nom, description, new ArrayList<Outil>());
		try {
			dos.saveCategorie(c);
		} catch (Exception e) {
			request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/addCategorie.jsp")
				.forward(request, response);
			e.printStackTrace();
			return;
		}
		response.sendRedirect("categories");
	}
}
