package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Outil;

@WebServlet("/outil")
public class OutilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("outil", new Outil("Maven", "Gestionnaire de build", "son url", null));
		request.getServletContext()
			.getRequestDispatcher("/WEB-INF/views/outil.jsp")
			.forward(request, response);
	}

}
