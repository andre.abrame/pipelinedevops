package test;

import java.util.ArrayList;
import java.util.Scanner;

import models.Categorie;
import models.Outil;
import services.DevOpsService;

public class Main {

	public static void main(String [] args) throws Exception {
		
		Scanner s = new Scanner(System.in);
		DevOpsService dos = new DevOpsService();
		
		int choix;
		final int QUIT = 0;
		do {
			// affiche le menu
			System.out.println("1. Afficher les categories");
			System.out.println("2. Ajouter une categorie");
			System.out.println("3. Modifier une categorie");
			System.out.println("4. Supprimer une categorie");
			System.out.println("5. Ajouter un outil");
			System.out.println(QUIT + ". Quit");
			// lit le choix de l'utilisateur 
			choix = Integer.parseInt(s.nextLine());
			// applique l'action choisie
			switch (choix) {
			case 1:
				// affichage avec un foreach
//				for (int i=0; i<dos.findAllCategorie().size(); ++i) {
//					Categorie c = dos.findCategorieById(i);
//					System.out.println(i+"\tnom : " + c.getNom() + "\n\tdescription : " + c.getDescription() + "\n\toutils :");
//					for (int j=0; j<c.getOutils().size(); ++j)
//						System.out.println("\t\t"+c.getOutils().get(j).getNom());
//				}
//				// affichage avec un foreach
				for (Categorie c : dos.findAllCategorie()) {
					System.out.println("id : " + c.getId() + "\nnom : " + c.getNom() + "\ndescription : " + c.getDescription() + "\n\toutils :");
				for (Outil o : c.getOutils())
				System.out.println("\t\t"+o.getNom());
				}
//				// afichage avec un stream
//				dos.findAllCategorie().stream()
//					.forEach(c -> System.out.println("nom : " + c.getNom() + "\ndescription : " + c.getDescription()));
//				// affichage "brut"
//				System.out.println(dos.findAllCategorie());
				break;
			case 2:
				{
					// demander à l'utilisateur les infos sur la categorie
					System.out.println("nom ?");
					String nom = s.nextLine();
					System.out.println("description ?");
					String description = s.nextLine();
					// creer la categorie
					Categorie c = new Categorie(nom, description, new ArrayList<Outil>());
					// persister la categorie
					dos.saveCategorie(c);
				}
				break;
			case 3:
				{
					System.out.println("id ?");
					int id = Integer.parseInt(s.nextLine());
					Categorie c = dos.findCategorieById(id);
					System.out.println("nom ["+c.getNom()+"] ?");
					String nom = s.nextLine();
					if (!nom.isEmpty())
						c.setNom(nom);
					System.out.println("description ["+c.getDescription()+"] ?");
					String description = s.nextLine();
					if (!description.isEmpty())
						c.setDescription(description);
					dos.updateCategorie(c);
				}
				break;
			case 4:
				{
					System.out.println("id ?");
					int index = Integer.parseInt(s.nextLine());
					dos.deleteCategorie(dos.findCategorieById(index));
				}
				break;
			case 5:
				{
					System.out.println("nom ?");
					String nom = s.nextLine();
					System.out.println("description ?");
					String description = s.nextLine();
					System.out.println("url ?");
					String url = s.nextLine();
					System.out.print("categorie ( ");
					for (Categorie c : dos.findAllCategorie())
						System.out.print(c.getId() +". "+c.getNom()+" ");
					System.out.println(") ?");
					Categorie c = dos.findCategorieById(Integer.parseInt(s.nextLine()));
					Outil o = new Outil(nom, description, url, c);
					c.getOutils().add(o);
					dos.saveOutil(o);
					dos.updateCategorie(c);
				}
				break;
			case QUIT:
				System.out.println("bye !");
				break;
			default:
				System.out.println("wrong choice, try again mate.");
			}
			
		} while (choix != QUIT); 
		
		s.close();
			
	}
	
	
}
