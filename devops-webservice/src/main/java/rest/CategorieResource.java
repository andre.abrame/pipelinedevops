package rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import models.Categorie;
import services.DevOpsService;

@Path("categorie")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class CategorieResource {

	private DevOpsService dos = new DevOpsService();
	
	@GET
	public Response findAll() {
		return Response
				.ok()
				.entity(new GenericEntity<List<Categorie>>(dos.findAllCategorie()) {})
				.build();
	}
	
	@GET
	@Path("{id}")
	public Response findById(@PathParam("id") int id) {
		return Response
				.ok()
				.entity(dos.findCategorieById(id))
				.build();
	}
	
	@POST
	public Response save(Categorie c) {
		try {
			dos.saveCategorie(c);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
		return Response.ok().build();
	}
	
	@PUT
	@Path("{id}")
	public Response update(@PathParam("id") int id, Categorie c) {
		try {
			c.setId(id);
			dos.updateCategorie(c);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
		return Response.ok().build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int id) {
		try {
			dos.deleteCategorie(dos.findCategorieById(id));
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
		return Response.ok().build();
	}
	
	
}
