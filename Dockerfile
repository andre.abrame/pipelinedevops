# image initiale
FROM my-ubuntu

# definition des parametres par défaut de la DB, pouvant etre ecrases en passant des variables d'environnement
ENV DB_URL=localhost:3306
ENV DB_USR=root
ENV DB_PWD=root
#RUN apt -y update && apt -y install git maven default-jdk wget
RUN git clone https://gitlab.com/andre.abrame/pipelinedevops.git
RUN wget http://mirrors.ircam.fr/pub/apache/tomcat/tomcat-9/v9.0.26/bin/apache-tomcat-9.0.26.tar.gz &&\
	tar xzvf apache-tomcat-9.0.26.tar.gz -C .
WORKDIR pipelinedevops
RUN echo "hibernate.connection.url = jdbc:mysql://${DB_URL}/pipelineDevOps?serverTimezone=Europe/Paris \
hibernate.connection.username = ${DB_USR} \
hibernate.connection.password = ${DB_PWD} \
showSql = true" > devops-core/src/main/resources/configuration.properties
RUN mvn install
RUN cp devops-webapp/target/devops-webapp.war ~/tomcat-9.0.26/webapps/
WORKDIR ~
ENTRYPOINT ./tomcat-9.0.26/bin/catalina.sh run