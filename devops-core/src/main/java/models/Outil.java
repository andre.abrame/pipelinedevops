package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Outil {

	@Id
	@GeneratedValue
	private int id;
	
	private String nom;
	
	private String description;
	
	private String url;
	
	@ManyToOne
	private Categorie categorie;
	
	
	
	public Outil() {
		super();
	}

	public Outil(String nom, String description, String url, Categorie categorie) {
		super();
		this.nom = nom;
		this.description = description;
		this.url = url;
		this.categorie = categorie;
	}

	public String getNom() {
		return nom;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}
	
	@XmlIDREF
	public Categorie getCategorie() {
		return categorie;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	@Override
	public String toString() {
		return "Outil [nom=" + nom + ", description=" + description + ", url=" + url + "]";
	}
	
}
