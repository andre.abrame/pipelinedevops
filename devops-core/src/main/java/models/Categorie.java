package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Categorie implements Comparable<Categorie> {
	
	@Id
	@GeneratedValue
	private int id;
	private String nom;
	private String description;
	@OneToMany(mappedBy="categorie", fetch=FetchType.EAGER)
	private List<Outil> outils = new ArrayList<Outil>();
	
	
	public Categorie() {
		super();
	}

	public Categorie(String nom, String description, List<Outil> outils) {
		super();
		this.nom = nom;
		this.description = description;
		this.outils.addAll(outils);
	}

	public String getNom() {
		return nom;
	}

	public String getDescription() {
		return description;
	}

	public List<Outil> getOutils() {
		return outils;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setOutils(List<Outil> outils) {
		this.outils = outils;
	}
	
	
	@XmlID
	public String getIdCategorie() {
		return ""+id;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((outils == null) ? 0 : outils.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categorie other = (Categorie) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (outils == null) {
			if (other.outils != null)
				return false;
		} else if (!outils.equals(other.outils))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Categorie o) {
		return this.nom.compareTo(o.nom);
	}

	@Override
	public String toString() {
		return "Categorie [nom=" + nom + ", description=" + description + ", outils=" + outils + "]";
	}
}
