package dao;

import models.Categorie;

public class CategorieDao extends GenericDao<Categorie> {

	public CategorieDao() {
		super(Categorie.class);
	}


}
