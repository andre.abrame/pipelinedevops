package services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import dao.CategorieDao;
import dao.OutilDao;
import models.Categorie;
import models.Outil;

public class DevOpsService {

	private CategorieDao cd = new CategorieDao();
	private OutilDao od = new OutilDao();
	
	
	public Categorie findCategorieById(int id) {
		return cd.findById(id);
	}
	
	public List<Categorie> findAllCategorie() {
		return cd.findAll();
	}
	
	public void saveCategorie(Categorie c) throws Exception {
		cd.save(c);
	}
	
	public void updateCategorie(Categorie c) throws Exception {
		cd.update(c);
	}
	
	public void deleteCategorie(Categorie c) throws Exception {
		cd.delete(c);
	}
	
	
	public Outil findOutilById(int id) {
		return od.findById(id);
	}
	
	public List<Outil> findAllOutils() {
		return od.findAll();
	}
	
	public void saveOutil(Outil o) throws Exception {
		od.save(o);
	}
	
	public void updateOutil(Outil o) throws Exception {
		od.update(o);
	}
	
	public void deleteOutil(Outil o) throws Exception {
		od.delete(o);
	}
	
}
